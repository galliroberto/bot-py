import requests
import settings

class Bot:
    url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'
    valuta = 'EUR'

    params = {
        'start': 1,
        'limit': 10,
        'convert': valuta,
    }

    headers = {
        'Accept': 'Application/json',
        'X-CMC_PRO_API_KEY': settings.CMC_PRO_API_KEY,
    }

    def ottieniValute(self):
        r = requests.get(url=self.url, params=self.params, headers=self.headers).json()
        return r['data']

