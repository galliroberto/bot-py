from dotenv import load_dotenv

load_dotenv()

# OR, the same with increased verbosity
load_dotenv(verbose=True)

# OR, explicitly providing path to '.env'
from pathlib import Path  # Python 3.6+ only

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

import os

CMC_PRO_API_KEY = os.getenv("CMC_PRO_API_KEY")
SALDO_INIZIALE = os.getenv("SALDO_INIZIALE")
NUMERO_MINIMO_DI_VALUTE_PER_ACQUISTARE = int(os.getenv("NUMERO_MINIMO_DI_VALUTE_PER_ACQUISTARE"))
CONDIZIONE_INCREMENTO_ACQUISTO = float(os.getenv("CONDIZIONE_INCREMENTO_ACQUISTO"))
CONDIZIONE_DECREMENTO_VENDITA = float(os.getenv("CONDIZIONE_DECREMENTO_VENDITA"))
MINUTI_PAUSA = int(os.getenv("MINUTI_PAUSA"))
MINUTI_TRASCORSI_PER_ACQUISTARE = int(os.getenv("MINUTI_TRASCORSI_PER_ACQUISTARE"))
