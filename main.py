from bot import Bot
from ordine import Ordine
from ordini import Ordini
import time
from datetime import datetime
from pprint import pprint
import sys
import settings

numeroMinimoDiValutePerAcquistare = settings.NUMERO_MINIMO_DI_VALUTE_PER_ACQUISTARE
condizioneIncrementoAcquisto = settings.CONDIZIONE_INCREMENTO_ACQUISTO
condizioneDecrementoVendita = settings.CONDIZIONE_DECREMENTO_VENDITA
minutiPausa = settings.MINUTI_PAUSA
minutiTrascorsiPerAcquistare = settings.MINUTI_TRASCORSI_PER_ACQUISTARE

numeroValuteInIntevalloTemporale = 0
tempoZero = 0
bot = Bot()
ordini = Ordini()

while (1):
    now = datetime.now()
    nowInSecondi = int(now.timestamp())
    currencies = bot.ottieniValute()
    tempoZero = nowInSecondi if 0 == tempoZero else tempoZero
    secondiPausa = 60 * minutiPausa
    secondiTrascorsiPerPoterAcquistare = 60 * minutiTrascorsiPerAcquistare

    pprint({
        'nowInSecondi': nowInSecondi,
        'tempoZero': tempoZero,
    })

    for currency in currencies:
        # print('variazione percentuala ultima ora {}: {}'.format(currency['symbol'], currency['quote'][bot.valuta]['percent_change_1h']))
        if currency['quote'][bot.valuta]['percent_change_1h'] > condizioneIncrementoAcquisto:
            if 0 == numeroValuteInIntevalloTemporale:
                valutaDaAcquistare = currency

            if currency['quote'][bot.valuta]['percent_change_1h'] > valutaDaAcquistare['quote'][bot.valuta]['percent_change_1h']:
                valutaDaAcquistare = currency

            numeroValuteInIntevalloTemporale += 1

        if ordini.possoVendereLaValuta(currency['symbol']):
            print('ho una valuta da vendere, verifico la condizione... {} < {}'.format(currency['quote'][bot.valuta]['percent_change_1h'], condizioneDecrementoVendita))
            if currency['quote'][bot.valuta]['percent_change_1h'] < condizioneDecrementoVendita:
                print('è ora di vendere {} {}'.format(currency['symbol'], currency['quote'][bot.valuta]['price']))
                ordini.eseguiVendita(
                    Ordine(
                        dataOrdine=datetime.now(),
                        tipoOrdine="vendita",
                        valuta=currency['symbol'],
                        prezzo=currency['quote'][bot.valuta]['price'],
                        daVendere=False
                    )
                )

    if (tempoZero + secondiTrascorsiPerPoterAcquistare >= nowInSecondi):
        print('è ora di verificare se posso fare una operazione di acquisto')
        print('numero valute in intervallo di tempo {}'.format(numeroValuteInIntevalloTemporale))
        if ordini.possoAcquistare() is True and numeroValuteInIntevalloTemporale >= numeroMinimoDiValutePerAcquistare:
            pprint('ho autorizzazione a procedere con acquisto di {} {}'.format(valutaDaAcquistare['symbol'], valutaDaAcquistare['quote'][bot.valuta]['price']))
            ordini.eseguiAcquisto(
                Ordine(
                    dataOrdine=datetime.now(),
                    tipoOrdine="acquisto",
                    valuta=valutaDaAcquistare['symbol'],
                    prezzo=valutaDaAcquistare['quote'][bot.valuta]['price'],
                    daVendere=True
                )
            )



        tempoZero = nowInSecondi
        numeroValuteInIntevalloTemporale = 0
        valutaDaAcquistare = {}

    time.sleep(secondiPausa)

    print('saldo {}'.format(ordini.ottieniSaldo()))
