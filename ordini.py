import settings

class Ordini:
    _saldoIniziale = settings.SALDO_INIZIALE
    ordini = []

    def possoAcquistare(self):
        for ordine in self.ordini:
            if True == ordine.daVendere:
                return False

        return True

    def possoVendereLaValuta(self, valuta):
        for ordine in self.ordini:
            if valuta == ordine.valuta and ordine.daVendere is True:
                return True

        return False

    def eseguiAcquisto(self, ordine):
        self.ordini.append(ordine)

    def eseguiVendita(self, ordine):
        self.ordini.append(ordine)

        for ordine in self.ordini:
            if True == ordine.daVendere:
                ordine.daVendere = False

    def ottieniSaldo(self):
        saldo = 0
        for ordine in self.ordini:
            if "acquisto" == ordine.tipoOrdine:
                saldo -= ordine.prezzo
            else:
                saldo += ordine.prezzo

        return self._saldoIniziale + saldo
